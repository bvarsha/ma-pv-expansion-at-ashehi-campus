import pandas as pd
import numpy as np
import math
import datetime as dt
import pvlib
import matplotlib.pyplot as plt
import math
import time
import cvxpy as cp
import pickle
import os
import sys
import logging

from matplotlib.cbook import boxplot_stats
import xlsxwriter


#
def import_capacities():
    input_path = 'system_config.xlsx'
    capacities_path = 'max_capacity_exp2.txt'

    energy_carriers = pd.read_excel(input_path, sheet_name='Energy Carriers', header=None, skiprows=3)
    energy_carriers = energy_carriers[0].tolist()

    conversion_info = pd.read_excel(input_path, sheet_name='Conversion Techs', header=None, skiprows=2)
    conversion_info = conversion_info.drop([0], axis=1).transpose()
    conversion_info = conversion_info.drop([1, 2, 3, 4, 5, 6, 7, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19], axis=1)

    storage_info = pd.read_excel(input_path, sheet_name='Storage Techs', header=None, skiprows=2)
    storage_info = storage_info.drop([0], axis=1).transpose()
    storage_info = storage_info.drop([1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15], axis=1)

    conversion_tech = conversion_info[0].tolist()
    conversion_eff = conversion_info[8].tolist()
    conversion_outshare = conversion_info[10].tolist()
    conversion_hub = conversion_info[20].tolist()

    storage_tech = storage_info[0].tolist()
    storage_hub = storage_info[16].tolist()
    storage_stateff = storage_info[9].tolist()
    storage_cyceff = storage_info[10].tolist()

    techlen = len(conversion_tech)
    tech_details = np.array(range(1, techlen + 1))
    tech_details = np.repeat(tech_details, 4)
    tech_details = tech_details.reshape(techlen, 4)
    tech_details = pd.DataFrame(tech_details, columns=['tech', 'eff', 'outshare', 'hub'])
    tech_details = tech_details.apply(pd.to_numeric)

    storelen = len(storage_tech)
    storage_details = np.array(range(1, storelen + 1))
    storage_details = np.repeat(storage_details, 4)
    storage_details = storage_details.reshape(storelen, 4)
    storage_details = pd.DataFrame(storage_details, columns=['tech', 'stateff', 'cyceff', 'hub'])
    storage_details = storage_details.apply(pd.to_numeric)

    for i in range(techlen):
        tech_details['tech'] = tech_details['tech'].replace(i + 1, conversion_tech[i])
        tech_details['eff'] = tech_details['eff'].replace(i + 1, conversion_eff[i])
        tech_details['outshare'] = tech_details['outshare'].replace(i + 1, conversion_outshare[i])
        tech_details['hub'] = tech_details['hub'].replace(i + 1, conversion_hub[i])
        i = i + 1

    for i in range(storelen):
        storage_details['tech'] = storage_details['tech'].replace(i + 1, storage_tech[i])
        storage_details['stateff'] = storage_details['stateff'].replace(i + 1, storage_stateff[i])
        storage_details['cyceff'] = storage_details['cyceff'].replace(i + 1, storage_cyceff[i])
        storage_details['hub'] = storage_details['hub'].replace(i + 1, storage_hub[i])
        i = i + 1

    lines = []
    cap_conv = []
    cap_stor = []

    with open(capacities_path, 'rt') as in_file:
        for line in in_file:
            lines.append(line)

    for row in lines:

        if row.find('CapTech') >= 0:
            row2 = row.split(sep=' ')
            cap_conv.append(row2[1:4])

        if row.find("CapStg") >= 0:
            row2 = row.split(sep=" ")
            cap_stor.append(row2[1:5])

    capacities_conv = pd.DataFrame(cap_conv, columns=['hub', 'tech', 'value'])
    capacities_conv = capacities_conv.apply(pd.to_numeric)
    capacities_stor = pd.DataFrame(cap_stor, columns=['hub', 'tech', 'drop', 'value'])
    capacities_stor = capacities_stor.apply(pd.to_numeric)
    i = 1
    for label in conversion_tech:
        capacities_conv['tech'] = capacities_conv['tech'].replace(i, label)
        i = i + 1

    i = 1
    for label in storage_tech:
        capacities_stor['tech'] = capacities_stor['tech'].replace(i, label)
        i = i + 1

    capacities_stor = capacities_stor.drop(['drop'], axis=1)
    capacities_stor = capacities_stor[capacities_stor.value != 0]
    capacities_conv = capacities_conv[capacities_conv.value != 0]

    return capacities_conv, capacities_stor, tech_details, storage_details


def network_capacities():
    input_path = 'system_config.xlsx'
    network_info = pd.read_excel(input_path, sheet_name='Network', header=None, skiprows=2)
    network_info = network_info.drop([0], axis=1).transpose()
    network_info = network_info.drop([2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15], axis=1)
    node1 = network_info[0].tolist()
    node2 = network_info[1].tolist()
    node_cap = network_info[6].tolist()
    bigM = 10 ** 7

    netlen = len(network_info)
    tech_details = np.array(range(bigM, (2 * netlen) + bigM))
    tech_details = np.repeat(tech_details, 3)
    tech_details = tech_details.reshape(2 * netlen, 3)
    tech_details = pd.DataFrame(tech_details, columns=['node1', 'node2', 'value'])
    tech_details = tech_details.apply(pd.to_numeric)

    for i in range(2 * netlen):
        if i < netlen:
            tech_details['node1'] = tech_details['node1'].replace(i + bigM, node1[i])
            tech_details['node2'] = tech_details['node2'].replace(i + bigM, node2[i])
            tech_details['value'] = tech_details['value'].replace(i + bigM, node_cap[i])
        else:
            tech_details['node1'] = tech_details['node1'].replace(i + bigM, node2[i - netlen])
            tech_details['node2'] = tech_details['node2'].replace(i + bigM, node1[i - netlen])
            tech_details['value'] = tech_details['value'].replace(i + bigM, node_cap[i - netlen])

    return tech_details


def CHP_operation(Pmax_CHP, Eff_CHP, PEff_CHP, QEff_CHP, n_CHP, min_cap_CHP):
    CHP_fuelcap = Pmax_CHP / Eff_CHP

    AP = n_CHP * min_cap_CHP * CHP_fuelcap
    AQ = 0

    BP = CHP_fuelcap * min_cap_CHP * PEff_CHP
    BQ = CHP_fuelcap * min_cap_CHP * QEff_CHP

    CP = CHP_fuelcap * PEff_CHP
    CQ = CHP_fuelcap * QEff_CHP

    DQ = AQ + (CQ - BQ)
    DP = AP + (CP - BP)

    EP = n_CHP * CHP_fuelcap
    EQ = 0

    CHP_points = np.array([[AQ, BQ, CQ, DQ, EQ], [AP, BP, CP, DP, EP]])

    return CHP_points


grid_config = {
    'high_tariff': 0.27,  # [CHF/kWh]
    'low_tariff': 0.22,  # [CHF/kWh]
    'feed_in_tariff': 0.12,  # [CHF/kWh]
}


def get_data(start_time):

    df_bc = pd.concat([pd.read_csv('demand_nestbldgs.csv', encoding='ISO-8859-1')])
    df_bc['timestamp'] = pd.date_range(start=start_time, periods=len(df_bc), tz='Europe/Zurich',
                                       freq="15min").to_pydatetime().tolist()  # Create data range with local time
    df_bc.set_index('timestamp', inplace=True)
    df_bc['dayinyear'] = df_bc.index.dayofyear

    df_bc['feed_in_tariff'] = grid_config['feed_in_tariff']
    df_bc['el_tariff'] = grid_config['low_tariff']

    df_bc.loc[((7 <= df_bc.index.hour) & (df_bc.index.hour <= 20) & (df_bc.index.dayofweek < 5)), 'el_tariff'] = \
        grid_config['high_tariff']
    df_bc.loc[((7 <= df_bc.index.hour) & (df_bc.index.hour <= 13) & (df_bc.index.dayofweek == 5)), 'el_tariff'] = \
        grid_config['high_tariff']
    return df_bc


def savePower(workbook, demand_power_final, power_final, grid_in_final, grid_out_final, storage_elecin_final,
              storage_elecout_final, p_avg_final, list_techs, list_storage, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final power')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        demstr = 'demand ' + str(i + 1)
        header.extend([demstr])
        header.extend(list_techs[i])
        header.extend(['grid input', 'grid output'])

        if 'Battery' in list_storage[i]:
            header.extend(['Battery input', 'Battery output'])

        for k in range(num_hubs):
            if k != i:
                templ = ['Hub ' + str(i + 1) + ' To' + str(k + 1)]
                header.extend(templ)

    # for k in range(num_hubs):
    #     if k != i:
    #         templ = ['From hub' + str(k + 1)]
    #         header.extend(templ)

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs):
            xl_data.extend([demand_power_final[i][j]])
            xl_data.extend(power_final[i][j])
            xl_data.extend([0])
            xl_data.extend([grid_in_final[i][j]])
            xl_data.extend([grid_out_final[i][j]])

            if 'Battery' in list_storage[j]:
                xl_data.extend([storage_elecin_final[i][j]])
                xl_data.extend([storage_elecout_final[i][j]])

            for idx1 in range(num_hubs - 1):
                xl_data.extend([p_avg_final[i][j * (num_hubs - 1) + idx1]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveHeat(workbook, demand_heat_final, heat_final, storage_heatin_final, storage_heatout_final, h_avg_final,
             list_techs, list_storage, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final heat')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        demstr = 'demand ' + str(i + 1)
        header.extend([demstr])
        header.extend(list_techs[i])

        if 'heat_storage' in list_storage[i]:
            header.extend(['Storage input', 'Storage output'])

        for k in range(num_hubs):
            if k != i:
                templ = ['Hub ' + str(i + 1) + ' To' + str(k + 1)]
                header.extend(templ)

        # for k in range(num_hubs):
        #     if k != i:
        #         templ = ['To hub' + str(k + 1)]
        #         header.extend(templ)
        #
        # for k in range(num_hubs):
        #     if k != i:
        #         templ = ['From hub' + str(k + 1)]
        #         header.extend(templ)

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs):
            xl_data.extend([demand_heat_final[i][j]])
            xl_data.extend(heat_final[i][j])
            xl_data.extend([0])

            if 'heat_storage' in list_storage[j]:
                xl_data.extend([storage_heatin_final[i][j]])
                xl_data.extend([storage_heatout_final[i][j]])

            for idx1 in range(num_hubs - 1):
                xl_data.extend([h_avg_final[i][j * (num_hubs - 1) + idx1]])

            # for idx1 in range(num_hubs - 1):
            #     xl_data.extend([heat_net_final[i][int(exp_list[j][idx1])][0]])
            #
            # for idx1 in range(num_hubs - 1):
            #     xl_data.extend([heat_net_final[i][int(imp_list[j][idx1])][1]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveCost(workbook, tech_cost_final, storage_cost_final, final_cost_final, grid_cost_final, transfer_cost_final,
             date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final cost')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        header.extend(['tech cost ' + str(i + 1), 'grid cost ' + str(i + 1),
                       'storage cost ' + str(i + 1), 'transfer cost ' + str(i + 1), 'final cost ' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs):
            xl_data.extend([tech_cost_final[i][j]])
            xl_data.extend([grid_cost_final[i][j]])
            xl_data.extend([storage_cost_final[i][j]])
            xl_data.extend([transfer_cost_final[i][j]])
            xl_data.extend([final_cost_final[i][j]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveStorage(workbook, storage_heat_final, storage_elec_final, SOC_elec, SOC_therm, storage_elecin_final,
                storage_elecout_final, storage_heatin_final, storage_heatout_final, list_storage, date_info,
                time_info, num_hubs):
    worksheet = workbook.add_worksheet('final storage')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        if 'heat_storage' in list_storage[i]:
            header.extend(['thermal level - hub ' + str(i + 1), 'thermal SOC - hub ' + str(i + 1),
                           'thermal inp - hub ' + str(i + 1), 'thermal out - hub ' + str(i + 1)])

        if 'Battery' in list_storage[i]:
            header.extend(['Battery level - hub ' + str(i + 1), 'Battery SOC - hub ' + str(i + 1),
                           'Battery inp - hub ' + str(i + 1), 'Battery out - hub ' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs):
            if 'heat_storage' in list_storage[j]:
                xl_data.extend([storage_heat_final[i][j]])
                xl_data.extend([SOC_therm[i][j]])
                xl_data.extend([storage_heatin_final[i][j]])
                xl_data.extend([storage_heatout_final[i][j]])

            if 'Battery' in list_storage[j]:
                xl_data.extend([storage_elec_final[i][j]])
                xl_data.extend([SOC_elec[i][j]])
                xl_data.extend([storage_elecin_final[i][j]])
                xl_data.extend([storage_elecout_final[i][j]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveChp(workbook, CHP_ontime_final, CHP_offtime_final, list_techs, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final CHP')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        if 'Gas_CHP_unit_2' in list_techs[i]:
            header.extend(['CHP on time - hub ' + str(i + 1), 'CHP off time - hub ' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs):
            if 'Gas_CHP_unit_2' in list_techs[j]:
                xl_data.extend([CHP_ontime_final[i][j]])
                xl_data.extend([CHP_offtime_final[i][j]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveiter(workbook, iter_final, date_info, time_info, num_hubs):

    worksheet = workbook.add_worksheet('iteration')

    header = []
    header.extend(['date', 'time', 'iteration'])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        xl_data.extend([iter_final[i]])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def savetrans(workbook, p_avg_final, z_list_P_final, lagran_P_final, h_avg_final, z_list_H_final, lagran_H_final,
              date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final transfer')

    header = []
    header.extend(['date', 'time'])

    for item in range(int(num_hubs * (num_hubs - 1))):
        header.extend(['z_P_' + str(item)])
        header.extend(['pavg_' + str(item)])
        for idx in range(num_hubs):
            header.extend(['lagran_P_' + str(item) + '_hub' + str(idx)])

    for item in range(int(num_hubs * (num_hubs - 1))):
        header.extend(['z_H_' + str(item)])
        header.extend(['havg_' + str(item)])
        for idx in range(num_hubs):
            header.extend(['lagran_H_' + str(item) + '_hub' + str(idx)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        for k in range(int(num_hubs * (num_hubs - 1))):
            xl_data.extend([z_list_P_final[i][k]])
            xl_data.extend([p_avg_final[i][k]])
            for idx in range(num_hubs):
                xl_data.extend([lagran_P_final[i][idx][k]])

        for k in range(int(num_hubs * (num_hubs - 1))):
            xl_data.extend([z_list_H_final[i][k]])
            xl_data.extend([h_avg_final[i][k]])
            for idx in range(num_hubs):
                xl_data.extend([lagran_H_final[i][idx][k]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def initTranlist(num_hubs):
    exp_list = []
    imp_list = []
    hub_list = []

    for id in range(num_hubs):
        exp_list.append([])
        imp_list.append([])
        hub_list.append((id + 1) * (num_hubs - 1))

    j = 0
    for id in range(num_hubs):
        for kd in range(num_hubs):
            if kd != id:
                exp_list[id].extend([j])
                j += 1

    start = -1
    for i in range(num_hubs):
        j = 0
        for k in range(num_hubs):
            if k == i:
                j = 1
            else:
                imp_list[i].extend([start + j + (num_hubs - 1) * k])
        start += 1
    return exp_list, imp_list, hub_list

def saveTime(workbook, time_total, date_info, time_info):
    worksheet = workbook.add_worksheet('final Time')

    header = []
    header.extend(['date', 'time', 'TimeTot'])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        xl_data.extend(time_total[i])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)

def get_NEST_data(time_start, time_end):
    downloaded_data = pd.read_excel(
        'C:/Users/beva/Documents/GitHub/NEST_Sim/NEST_data/irradiance_data_meteo_swiss_raw.xlsx',
        sheet_name='data')  # irradiance_data_meteo_swiss_raw
    downloaded_data = downloaded_data.drop(['time'], axis=1)
    downloaded_data['formatted_time'] = pd.to_datetime(downloaded_data['formatted_time'], dayfirst=True)
    weather = downloaded_data.set_index('formatted_time')

    # point to change time of optimization
    weather = weather.loc[(weather.index >= '2018-01-01 00:00:00') & (weather.index < '2020-01-01 00:00:00')]
    weather = weather.rename({'gre000h0': 'ghi', 'ods000h0': 'dhi', 'tre200h0': 'temp_air'}, axis=1)
    weather['ghi'] = weather['ghi']
    weather['dhi'] = weather['dhi']
    weather['wind_speed'] = 5

    # time for zenith calculation with minute resolution
    time = pd.date_range(start=weather.index[0] - pd.Timedelta(minutes=59), end=weather.index[-1], freq="1min")

    # time for zenith calc with hour mid-point (:30)
    # time = pd.date_range(start=weather.index[0]-pd.Timedelta(minutes=30), end=weather.index[-1], freq="1H")

    # calculate zenith, assumes UTC unless localized
    # position = pvlib.solarposition.get_solarposition(time=weather.index ,latitude=47.403, longitude=8.613,altitude=433,temperature=weather['temp_air'])
    position = pvlib.solarposition.get_solarposition(time=time, latitude=47.403, longitude=8.613, altitude=433)
    position = position.resample('1H', origin='end').mean()  # ->pick the one in the middle, at :30
    # position.index = position.index+pd.Timedelta(hours=1)
    zenith = position['apparent_zenith']
    azimuth = position['azimuth']
    weather['zenith'] = position['apparent_zenith'].values
    weather['azimuth'] = position['azimuth'].values

    # add dni
    weather['dni'] = pvlib.irradiance.dni(ghi=weather['ghi'], dhi=weather['dhi'], zenith=weather['zenith'])
    # replce nan with 0
    weather['dni'] = weather['dni'].fillna(0)

    # get dni based on ghi
    weather['dni_2'] = pvlib.irradiance.dirint(ghi=weather['ghi'], solar_zenith=weather['zenith'], times=position.index,
                                               pressure=96000.0, use_delta_kt_prime=False)
    # replce nan with 0
    weather['dni_2'] = weather['dni_2'].fillna(0)

    # calculate dhi based on ghi and dni
    weather['dhi_2'] = 0
    weather.loc[weather['dni_2'] > 0, 'dhi_2'] = weather.loc[weather['dni_2'] > 0, 'ghi'].values - weather.loc[
        weather['dni_2'] > 0, 'dni_2'].values * np.cos(weather.loc[weather['dni_2'] > 0, 'zenith'].values / 180 * np.pi)

    # add extra dni
    weather['dni_extra'] = pvlib.irradiance.get_extra_radiation(datetime_or_doy=weather.index, epoch_year=2019)

    # add relative airmass
    weather['airmass_rel'] = pvlib.atmosphere.get_relative_airmass(weather['zenith'], model='kastenyoung1989')

    part_1_f = pvlib.irradiance.get_total_irradiance(surface_tilt=90, surface_azimuth=228,
                                                     solar_zenith=weather['zenith'],
                                                     solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                     ghi=weather['ghi'], dhi=weather['dhi'],
                                                     dni_extra=weather['dni_extra'],
                                                     airmass=weather['airmass_rel'], model='perez')
    I_Sol1 = 0.001 * part_1_f['poa_global']
    I_Sol1 = I_Sol1.loc[(I_Sol1.index >= time_start) & (I_Sol1.index < (time_end + dt.timedelta(hours=73)))]

    part_1 = pvlib.irradiance.get_total_irradiance(surface_tilt=10, surface_azimuth=125, solar_zenith=weather['zenith'],
                                                   solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                   ghi=weather['ghi'],
                                                   dhi=weather['dhi'], dni_extra=weather['dni_extra'],
                                                   airmass=weather['airmass_rel'], model='perez')
    part_2 = pvlib.irradiance.get_total_irradiance(surface_tilt=10, surface_azimuth=305, solar_zenith=weather['zenith'],
                                                   solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                   ghi=weather['ghi'],
                                                   dhi=weather['dhi'], dni_extra=weather['dni_extra'],
                                                   airmass=weather['airmass_rel'], model='perez')
    I_Sol2 = 0.001 * (0.5 * part_1['poa_global'] + 0.5 * part_2['poa_global'])
    I_Sol2 = I_Sol2.loc[(I_Sol2.index >= time_start) & (I_Sol2.index < (time_end + dt.timedelta(hours=73)))]

    part_1_f = pvlib.irradiance.get_total_irradiance(surface_tilt=90, surface_azimuth=208,
                                                     solar_zenith=weather['zenith'],
                                                     solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                     ghi=weather['ghi'], dhi=weather['dhi'],
                                                     dni_extra=weather['dni_extra'],
                                                     airmass=weather['airmass_rel'], model='perez')
    I_Sol3_1 = 0.001 * part_1_f['poa_global']
    I_Sol3_1 = I_Sol3_1.loc[(I_Sol3_1.index >= time_start) & (I_Sol3_1.index < (time_end + dt.timedelta(hours=73)))]

    part_2_f = pvlib.irradiance.get_total_irradiance(surface_tilt=90, surface_azimuth=118,
                                                     solar_zenith=weather['zenith'],
                                                     solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                     ghi=weather['ghi'], dhi=weather['dhi'],
                                                     dni_extra=weather['dni_extra'],
                                                     airmass=weather['airmass_rel'], model='perez')
    I_Sol3_2 = 0.001 * part_2_f['poa_global']
    I_Sol3_2 = I_Sol3_2.loc[(I_Sol3_2.index >= time_start) & (I_Sol3_2.index < (time_end + dt.timedelta(hours=73)))]

    part_1 = pvlib.irradiance.get_total_irradiance(surface_tilt=22, surface_azimuth=118, solar_zenith=weather['zenith'],
                                                   solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                   ghi=weather['ghi'],
                                                   dhi=weather['dhi'], dni_extra=weather['dni_extra'],
                                                   airmass=weather['airmass_rel'], model='perez')
    part_2 = pvlib.irradiance.get_total_irradiance(surface_tilt=22, surface_azimuth=298, solar_zenith=weather['zenith'],
                                                   solar_azimuth=weather['azimuth'], dni=weather['dni'],
                                                   ghi=weather['ghi'],
                                                   dhi=weather['dhi'], dni_extra=weather['dni_extra'],
                                                   airmass=weather['airmass_rel'], model='perez')
    I_Sol3_3 = 0.001 * (0.5 * part_1['poa_global'] + 0.5 * part_2['poa_global'])
    I_Sol3_3 = I_Sol3_3.loc[(I_Sol3_3.index >= time_start) & (I_Sol3_3.index < (time_end + dt.timedelta(hours=73)))]

    demand_data = pd.read_excel('C:/Users/beva/Documents/GitHub/NEST_Sim/NEST_data/demand_thermal_2018.xlsx',
                                sheet_name='data')  # irradiance_data_meteo_swiss_raw
    demand_data['UTC'] = pd.to_datetime(demand_data['UTC'], dayfirst=True)
    demand_thermal = demand_data.set_index('UTC')

    demand_mid_thermal = demand_thermal['mid_T_Total']
    demand_mid_thermal = demand_mid_thermal.loc[
        (demand_mid_thermal.index >= time_start) & (demand_mid_thermal.index < (time_end + dt.timedelta(hours=73)))]

    demand_high_thermal = demand_thermal['high_T_Total']
    demand_high_thermal = demand_high_thermal.loc[
        (demand_high_thermal.index >= time_start) & (demand_high_thermal.index < (time_end + dt.timedelta(hours=73)))]

    demand_data = pd.read_excel('C:/Users/beva/Documents/GitHub/NEST_Sim/NEST_data/demand_electricity_2018.xlsx',
                                sheet_name='data')  # irradiance_data_meteo_swiss_raw
    demand_data['UTC'] = pd.to_datetime(demand_data['UTC'], dayfirst=True)
    demand_elec = demand_data.set_index('UTC')
    demand_elec['feed_in_tariff'] = grid_config['feed_in_tariff']
    demand_elec['el_tariff'] = grid_config['low_tariff']
    demand_elec.loc[
        ((7 <= demand_elec.index.hour) & (demand_elec.index.hour <= 20) & (
                demand_elec.index.dayofweek < 5)), 'el_tariff'] = \
        grid_config['high_tariff']
    demand_elec.loc[((7 <= demand_elec.index.hour) & (demand_elec.index.hour <= 13) & (
            demand_elec.index.dayofweek == 5)), 'el_tariff'] = \
        grid_config['high_tariff']
    demand_elec = demand_elec.loc[
        (demand_elec.index >= time_start) & (demand_elec.index < (time_end + dt.timedelta(hours=73)))]
    demand_electric = demand_elec['aggregated_electricity_demand']
    return I_Sol1, I_Sol2, I_Sol3_1, I_Sol3_2, I_Sol3_3, demand_electric, demand_mid_thermal, demand_high_thermal

def savePower_real(workbook, demand_power_final, power_final, grid_in_final, grid_out_final, storage_elecin_final,
              storage_elecout_final, p_avg_final, list_techs, list_storage, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final power')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs-1):
        demstr = 'demand ' + str(i + 1)
        header.extend([demstr])
        header.extend(list_techs[i])
        header.extend(['grid input', 'grid output'])

        if 'Battery' in list_storage[i]:
            header.extend(['Battery input', 'Battery output'])

        for k in range(num_hubs):
            if k != i:
                templ = ['Hub ' + str(i + 1) + ' To' + str(k + 1)]
                header.extend(templ)

    demstr = 'demand_nest'
    header.extend([demstr])
    header.extend(['PV1', 'PV2', 'PV3', 'HP', 'HP high', 'grid input', 'grid output', 'Battery input', 'Battery output',
                   'Hub NEST to 1', 'Hub NEST to 2', 'Hub NEST to 3'])


    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs-1):
            xl_data.extend([demand_power_final[i][j]])
            xl_data.extend(power_final[i][j])
            xl_data.extend([0])
            xl_data.extend([grid_in_final[i][j]])
            xl_data.extend([grid_out_final[i][j]])

            if 'Battery' in list_storage[j]:
                xl_data.extend([storage_elecin_final[i][j]])
                xl_data.extend([storage_elecout_final[i][j]])

            for idx1 in range(num_hubs - 1):
                xl_data.extend([p_avg_final[i][j * (num_hubs - 1) + idx1]])

        j = j + 1
        xl_data.extend([demand_power_final[i][j]])
        xl_data.extend(power_final[i][j])
        xl_data.extend([grid_in_final[i][j]])
        xl_data.extend([grid_out_final[i][j]])
        xl_data.extend([storage_elecin_final[i][j]])
        xl_data.extend([storage_elecout_final[i][j]])

        for idx1 in range(num_hubs - 1):
            xl_data.extend([p_avg_final[i][j * (num_hubs - 1) + idx1]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveHeat_real(workbook, demand_heat_final, heat_final, storage_heatin_final, storage_heatout_final, h_avg_final,
             list_techs, list_storage, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final heat')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs-1):
        demstr = 'demand ' + str(i + 1)
        header.extend([demstr])
        header.extend(list_techs[i])

        if 'heat_storage' in list_storage[i]:
            header.extend(['Storage input', 'Storage output'])

        for k in range(num_hubs):
            if k != i:
                templ = ['Hub ' + str(i + 1) + ' To' + str(k + 1)]
                header.extend(templ)

    demstr = 'demand_nest'
    header.extend([demstr])
    header.extend(['PV1', 'PV2', 'PV3', 'HP', 'HP_high', 'Heat Exch', 'Storage input', 'Storage output',
                   'Hub NEST to 1', 'Hub NEST to 2', 'Hub NEST to 3'])
    header = [header]


    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs-1):
            xl_data.extend([demand_heat_final[i][j]])
            xl_data.extend(heat_final[i][j])
            xl_data.extend([0])

            if 'heat_storage' in list_storage[j]:
                xl_data.extend([storage_heatin_final[i][j]])
                xl_data.extend([storage_heatout_final[i][j]])

            for idx1 in range(num_hubs - 1):
                xl_data.extend([h_avg_final[i][j * (num_hubs - 1) + idx1]])

        j = j + 1
        xl_data.extend([demand_heat_final[i][j]])
        xl_data.extend(heat_final[i][j])
        xl_data.extend([storage_heatin_final[i][j]])
        xl_data.extend([storage_heatout_final[i][j]])

        for idx1 in range(num_hubs - 1):
            xl_data.extend([h_avg_final[i][j * (num_hubs - 1) + idx1]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveHeatLow_real(workbook, heat_grid_out_final, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('Heat Low')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        header.extend(['Heat low' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        total = 0
        for j in range(num_hubs):
            xl_data.extend([heat_grid_out_final[i][j]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveHeatHigh_real(workbook, demand_heat_high_final, heat_high_final, storage_heatin_final, storage_heatout_final,
                      date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('Heat High')

    header = []
    header.extend(['date', 'time'])
    header.extend(['demand high', 'HP high', 'Heat Exch', 'Storage input ', 'Storage output'])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        xl_data.extend(demand_heat_high_final[i])
        xl_data.extend(heat_high_final[i])
        xl_data.extend([storage_heatin_final[i][num_hubs]])
        xl_data.extend([storage_heatout_final[i][num_hubs]])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveCost_real(workbook, tech_cost_final, storage_cost_final, final_cost_final, grid_cost_final, transfer_cost_final,
             date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final cost')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs):
        header.extend(['tech cost ' + str(i + 1), 'grid cost ' + str(i + 1),
                       'storage cost ' + str(i + 1), 'transfer cost ' + str(i + 1), 'final cost ' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        total = 0
        for j in range(num_hubs):
            xl_data.extend([tech_cost_final[i][j]])
            xl_data.extend([grid_cost_final[i][j]])
            xl_data.extend([storage_cost_final[i][j]])
            xl_data.extend([transfer_cost_final[i][j]])
            xl_data.extend([final_cost_final[i][j]])
            total = total + final_cost_final[i][j]
        xl_data.extend([total])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveStorage_real(workbook, storage_heat_final, storage_elec_final, SOC_elec, SOC_therm, storage_elecin_final,
                storage_elecout_final, storage_heatin_final, storage_heatout_final, list_storage, date_info,
                time_info, num_hubs):
    worksheet = workbook.add_worksheet('final storage')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs-1):
        if 'heat_storage' in list_storage[i]:
            header.extend(['thermal level - hub ' + str(i + 1), 'thermal SOC - hub ' + str(i + 1),
                           'thermal inp - hub ' + str(i + 1), 'thermal out - hub ' + str(i + 1)])

        if 'Battery' in list_storage[i]:
            header.extend(['Battery level - hub ' + str(i + 1), 'Battery SOC - hub ' + str(i + 1),
                           'Battery inp - hub ' + str(i + 1), 'Battery out - hub ' + str(i + 1)])

    i = i + 1
    header.extend(['thermal level - hub ' + str(i + 1), 'thermal SOC - hub ' + str(i + 1),
                   'thermal inp - hub ' + str(i + 1), 'thermal out - hub ' + str(i + 1)])
    header.extend(
        ['Battery level - hub ' + str(i + 1), 'Battery SOC - hub ' + str(i + 1),
         'Battery inp - hub ' + str(i + 1), 'Battery out - hub ' + str(i + 1)])

    header.extend(['thermal high level - hub ' + str(i + 1), 'thermal high SOC - hub ' + str(i + 1),
                   'thermal high inp - hub ' + str(i + 1), 'thermal high out - hub ' + str(i + 1)])
    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs-1):
            if 'heat_storage' in list_storage[j]:
                xl_data.extend([storage_heat_final[i][j]])
                xl_data.extend([SOC_therm[i][j]])
                xl_data.extend([storage_heatin_final[i][j]])
                xl_data.extend([storage_heatout_final[i][j]])

            if 'Battery' in list_storage[j]:
                xl_data.extend([storage_elec_final[i][j]])
                xl_data.extend([SOC_elec[i][j]])
                xl_data.extend([storage_elecin_final[i][j]])
                xl_data.extend([storage_elecout_final[i][j]])

        j = j + 1
        xl_data.extend([storage_heat_final[i][j]])
        xl_data.extend([SOC_therm[i][j]])
        xl_data.extend([storage_heatin_final[i][j]])
        xl_data.extend([storage_heatout_final[i][j]])
        xl_data.extend([storage_elec_final[i][j]])
        xl_data.extend([SOC_elec[i][j]])
        xl_data.extend([storage_elecin_final[i][j]])
        xl_data.extend([storage_elecout_final[i][j]])
        xl_data.extend([storage_heat_final[i][j+1]])
        xl_data.extend([SOC_therm[i][j+1]])
        xl_data.extend([storage_heatin_final[i][j+1]])
        xl_data.extend([storage_heatout_final[i][j+1]])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveChp_real(workbook, CHP_ontime_final, CHP_offtime_final, list_techs, date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final CHP')

    header = []
    header.extend(['date', 'time'])

    for i in range(num_hubs-1):
        if 'Gas_CHP_unit_2' in list_techs[i]:
            header.extend(['CHP on time - hub ' + str(i + 1), 'CHP off time - hub ' + str(i + 1)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])

        for j in range(num_hubs-1):
            if 'Gas_CHP_unit_2' in list_techs[j]:
                xl_data.extend([CHP_ontime_final[i][j]])
                xl_data.extend([CHP_offtime_final[i][j]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def saveiter_real(workbook, iter_final, date_info, time_info, num_hubs):

    worksheet = workbook.add_worksheet('iteration')

    header = []
    header.extend(['date', 'time', 'iteration'])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        xl_data.extend([iter_final[i]])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def savetrans_real(workbook, p_avg_final, z_list_P_final, lagran_P_final, h_avg_final, z_list_H_final, lagran_H_final,
              date_info, time_info, num_hubs):
    worksheet = workbook.add_worksheet('final transfer')

    header = []
    header.extend(['date', 'time'])

    for item in range(int(num_hubs * (num_hubs - 1))):
        header.extend(['z_P_' + str(item)])
        header.extend(['pavg_' + str(item)])
        for idx in range(num_hubs):
            header.extend(['lagran_P_' + str(item) + '_hub' + str(idx)])

    for item in range(int(num_hubs * (num_hubs - 1))):
        header.extend(['z_H_' + str(item)])
        header.extend(['havg_' + str(item)])
        for idx in range(num_hubs):
            header.extend(['lagran_H_' + str(item) + '_hub' + str(idx)])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        for k in range(int(num_hubs * (num_hubs - 1))):
            xl_data.extend([z_list_P_final[i][k]])
            xl_data.extend([p_avg_final[i][k]])
            for idx in range(num_hubs):
                xl_data.extend([lagran_P_final[i][idx][k]])

        for k in range(int(num_hubs * (num_hubs - 1))):
            xl_data.extend([z_list_H_final[i][k]])
            xl_data.extend([h_avg_final[i][k]])
            for idx in range(num_hubs):
                xl_data.extend([lagran_H_final[i][idx][k]])

        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)


def initTranlist_real(num_hubs):
    exp_list = []
    imp_list = []
    hub_list = []

    for id in range(num_hubs):
        exp_list.append([])
        imp_list.append([])
        hub_list.append((id + 1) * (num_hubs - 1))

    j = 0
    for id in range(num_hubs):
        for kd in range(num_hubs):
            if kd != id:
                exp_list[id].extend([j])
                j += 1

    start = -1
    for i in range(num_hubs):
        j = 0
        for k in range(num_hubs):
            if k == i:
                j = 1
            else:
                imp_list[i].extend([start + j + (num_hubs - 1) * k])
        start += 1
    return exp_list, imp_list, hub_list

def saveTime_real(workbook, time_total, date_info, time_info):
    worksheet = workbook.add_worksheet('final Time')

    header = []
    header.extend(['date', 'time', 'TimeTot'])

    header = [header]

    for row_num, item in enumerate(header):
        worksheet.write_row(row_num, 0, item)

    xl_final = []

    for i in range(len(date_info)):
        xl_data = []
        xl_data.extend(date_info[i])
        xl_data.extend(time_info[i])
        xl_data.extend(time_total[i])
        xl_final.append(xl_data)

    for row_num, item in enumerate(xl_final, 1):
        worksheet.write_row(row_num, 0, item)